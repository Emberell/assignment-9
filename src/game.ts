/////////////////////////////////////
// Building in Decentraland course
// Lesson 9 scene to fix
// 
// this scene started out as a DCL 2.2.6, SDK 5.0.0 "dcl init" scene, as follows:
/*
npm rm -g decentraland
npm i -g decentraland@2.2.5
md Lesson9a-SDK5-FixMe
cd Lesson9a-SDK5-FixMe
dcl init
npm rm decentraland-ecs
npm i decentraland-ecs@5.0.0
// imported an SDK5 version of spawnerFunctions.ts module
// got busy reworking the game.ts file to have the basis for the Lesson assignment
*/

import {spawnPlaneX, spawnBoxX, spawnGltfX} from './modules/spawnerFunctions'

//const ground = spawnPlaneX (5,0,5,  90,0,0,  10,10,1)
//ground.addComponent(new Transform({ position: new Vector3(0, 180, 0) }))
//const groundMaterial = new Material()
//groundMaterial.albedoColor = new Color3(0.1, 0.4, 0)
//ground.addComponent(groundMaterial)
//engine.addEntity(ground)
//ground.getComponent(Transform).scale.set(4, 4, 4)

const box2 = spawnGltfX(new GLTFShape("models/Lesson9Cube/lesson9grass.glb"), 8,0,8, 90,180,0, 8,8,0.1)
//box2.addComponentOrReplace(new Transform({ position: new Vector3(0, 180, 0) }))
engine.addEntity(box2)

const box = spawnBoxX(3,1,4, 0,180,0,  1,1,1)
engine.addEntity(box)
const groundMaterial = new Material()
box.addComponent(groundMaterial)
//groundMaterial.albedoColor = new Color3(0.1, 0.4, 0)
//groundMaterial.metallic = 1
//groundMaterial.roughness = 0
groundMaterial.albedoColor = new Color3(0,0.2,0)
groundMaterial.metallic = 1
groundMaterial.roughness =0
//box2.addComponent(groundMaterial)



//box.addComponent(new Transform({ position: new Vector3(0, 180, 0) }))


const cube = spawnGltfX(new GLTFShape("models/Lesson9Cube/Lesson9Cube.glb"), 3,1,3, 0,180,0, 1,1,1)
//cube.addComponent(new Transform({ position: new Vector3(0, 180, 0) }))
engine.addEntity(cube)

const naturePack = spawnGltfX(new GLTFShape("models/NaturePack/Nature-Pack-gltf-v1.3.gltf.glb"), 11,0,2, 0,180,0,  0.3,0.3,0.3)
//naturePack.addComponent(new Transform({ position: new Vector3(0, 180, 0) }))
engine.addEntity(naturePack)

const firTree = spawnGltfX(new GLTFShape("models/TreeA_Fir/TreeA_Optimised_28_June_2018_A01.babylon.gltf"), 8,0,8, 0,180,0,  .2,.2,.2)
//firTree.addComponent(new Transform({ position: new Vector3(0, 180, 0) }))
//engine.addEntity(firTree)

const bird = spawnGltfX(new GLTFShape("models/Sparrow/Sparrow-burung-pipit.gltf"), 5,0.3,2, 0,90,0, 0.05, 0.05, 0.05)
//bird.addComponent(new Transform({ position: new Vector3(0, 180, 0) }))
engine.addEntity(bird)


